#! /usr/bin/env python
'''
Created on Mar 3, 2015

@author: markus
'''

import time,os,sys,getpass,getopt
import json as jsonhandler

if __name__ == "__main__":
    """
    Append source directory to PTYHONPATH if not yet done
    """
    sourcedir = os.path.abspath(os.path.dirname(sys.argv[0]))
    sourcedir = sourcedir.replace("/%s" %(os.path.join("Datatransfer", "TransferRequests")),"")
    if not sourcedir in sys.path:
        print "Adding %s to the PYTHONPATH" %(sourcedir)
        sys.path.append(sourcedir)

from Datatransfer.Struct.TransferJob import TransferJob
from Datatransfer.TransferDB.TransferDBConnector import TransferDBConnector
from Datatransfer.PyAlien.AlienTools import alien_find

class AlienConfigParam(object):
    '''
    Class storing the parameter set for an alien transfer request
    '''

    def __init__(self):
        self.__name = ''
        self.__alienbasefolder = ''
        self.__localbasefolder = ''
        self.__aliensearchpattern = ''
        self.__runlist = []

    def SetName(self, name):
        self.__name = name

    def SetLocalBasefolder(self, localbase):
        self.__localbasefolder = localbase

    def SetAlienBasefolder(self, alienbase):
        self.__alienbasefolder = alienbase

    def SetAlienSearchpattern(self, searchpattern):
        self.__aliensearchpattern = searchpattern

    def AddRun(self, runnumber):
        if not runnumber in self.__runlist:
            self.__runlist.append(runnumber)

    def SetRunlist(self, runlist):
        for run in runlist:
            self.AddRun(run)

    def GetName(self):
        return self.__name

    def GetLocalBasefolder(self):
        return self.__localbasefolder

    def GetAlienBasefolder(self):
        return self.__alienbasefolder

    def GetAlienSearchpattern(self):
        return self.__aliensearchpattern

    def GetRunlist(self):
        return self.__runlist

class AlienConfigReader(object):

    def __init__(self, filename):
        self.__alienparams = AlienConfigParam()
        self.__ReadFile(filename)

    def __ReadFile(self, filename):
        jsonstring = ''
        confighandler = open(filename, 'r')
        for line in confighandler:
            line = line.replace("\n", "");
            jsonstring += line
        confighandler.close()
        print "Processing request for config: %s" %(jsonstring)
        jsondict = jsonhandler.loads(jsonstring)
        for key, value in jsondict.iteritems():
            if key == "name":
                self.__alienparams.SetName(value)
            elif key == "alienbase":
                self.__alienparams.SetAlienBasefolder(value)
            elif key == "localbase":
                self.__alienparams.SetLocalBasefolder(value)
            elif key == "aliensearchpattern":
                self.__alienparams.SetAlienSearchpattern(value)
            elif key == "runlist":
                self.__alienparams.SetRunlist(value)

    def GetAlienParams(self):
        return self.__alienparams

class AlienTransferRequest(object):
    '''
    Standard transfer request for alien transfers
    '''

    def __init__(self, configuration):
        '''
        Constructor
        '''
        self.__configfile = configuration
        self.__transferDBentry = self.__Convert()

    def InsertIntoDatabase(self, connectionparams):
        '''
        Put the entry into the database
        '''
        dbconnection = TransferDBConnector(connectionparams)
        dbconnection.InsertTransferJob(self.__transferDBentry)

    def Print(self):
        '''
        Print the transfer request just created
        '''
        print "New transfer request:"
        self.__transferDBentry.Print()

    def __Convert(self):
        '''
        Converts the request into a transfer DB entry
        Performs also alien query
        '''
        configreader = AlienConfigReader(self.__configfile)
        aliconf = configreader.GetAlienParams()
        aliendbentry = TransferJob(aliconf.GetName())
        aliendbentry.SetIssuer(getpass.getuser())
        aliendbentry.SetCreationDate(time.localtime())
        for r in aliconf.GetRunlist():
            aliencollection = alien_find("%s/%s" %(aliconf.GetAlienBasefolder(), r), aliconf.GetAlienSearchpattern())
            for infile in aliencollection:
                outfile = infile
                outfile = outfile.replace(aliconf.GetAlienBasefolder(), aliconf.GetLocalBasefolder())
                aliendbentry.AddFile("alien://%s" %(infile), outfile)
        return aliendbentry


def ProcessAlienRequest(configfile, dbconnection):
    """
    Run an alien transferrequest
    """
    transferrequest = AlienTransferRequest(configfile)
    transferrequest.Print()
    transferrequest.InsertIntoDatabase(dbconnection)

def Usage():
    """
    Help function
    """
    print "Usage:"
    print "  AlienTransferRequest.py [OPTIONS]"
    print "        "
    print "Options:"
    print "  -c: config file (in JSON-format for the request)"
    print "  -d: connection string to the MongoDB"

if __name__ == "__main__":
    if len(sys.argv) < 2:
        Usage()
        sys.exit(1)
    configfile = ""
    dbconnection = ""
    opt,arg = getopt.getopt(sys.argv[1:], "c:d:h", [])

    for o,a in opt:
        if o == "-c":
            configfile = str(a)
        elif o == "-d":
            dbconnection = str(a)
        elif o == "-h":
            Usage()
            sys.exit(2)

    if not len(configfile):
        print "Configfile needs to be specified"
        sys.exit(1)

    if not len(dbconnection):
        print "Connection to mongodb needs to be specified"
        sys.exit(1)

    ProcessAlienRequest(configfile, dbconnection)
