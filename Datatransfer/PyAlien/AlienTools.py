'''
Created on Jan 19, 2015

Access to alien file catalogue operations in python

@author: markus
'''

from commands import getstatusoutput
import os
from subprocess import call as spcall
from Datatransfer.PyAlien.AlienFileCatalogueEntry import FileEntryDecoder

class AlienException(Exception):
    """
    Error handling for alien file catalogue operations
    """

    def __init__(self, sender, errorsource, producer):
        """
        Constructor
        """
        self.__sender = sender
        self.__errorsource = errorsource
        self.__producer = producer

    def GetSender(self):
        return self.__sender

    def GetProducer(self):
        return self.__producer

    def GetErrorCode(self):
        return self.__errorsource

class AlienLsWrapper(object):
    """
    Wrapper for alien_ls
    Performs alien_ls -l and interprets output as meta information
    Stores the data in a list
    """

    def __init__(self, inputdir):
        """
        Constructor
        """
        self.__directorycontent = self.__Perform(inputdir)

    def GetDirectoryContent(self):
        """
        Access to directory content (path + meta information)
        """
        return self.__directorycontent

    def __Perform(self, inputdir):
        """
        Run the alien_ls, make format processable, interpret output
        """
        entries = []
        readcommand = "alien_ls -l %s" %(inputdir)
        tmpstring = getstatusoutput(readcommand)[1]
        #replace all nasty formatting
        linesIn = tmpstring.split("\n")
        for inputline in linesIn:
            mytmpstring = inputline
            mytmpstring = mytmpstring.replace("\x1b[49;31m","")           # red
            mytmpstring = mytmpstring.replace("\x1b[49;32m","")           # green
            mytmpstring = mytmpstring.replace("\x1b[49;34m","")           # blue
            mytmpstring = mytmpstring.replace("\x1b[0m","")
            mytmpstring = ' '.join(mytmpstring.split())                   # strip whitespaces
            if not len(mytmpstring):
                continue
            decoder = FileEntryDecoder(mytmpstring)
            entries.append(decoder.GetEntry())
        return entries


def alien_ls(inputpath):
    """
    Make ls in alien
    Returs the content of a directory + meta information
    """
    if not alien_availability_test():
        raise AlienException("alien_find", "AlienNotAvailable", "")
    wrapper = AlienLsWrapper(inputpath)
    return wrapper.GetDirectoryContent()

def alien_is_file(path):
    """
    Check if content in the alien filecatalogue is a physical file
    """
    if not alien_availability_test():
        raise AlienException("alien_find", "AlienNotAvailable", "")
    content = alien_ls(path)
    if not len(content):
        return False
    if len(content) > 1:
        return False
    return content[0].GetUserRights().IsFile()

def alien_remove_file(entry):
    """
    Remove single file from the alien file catalogue
    Allows to delete content only inside home
    """
    if not alien_availability_test():
        raise AlienException("alien_find", "AlienNotAvailable", "")
    alienhome = alien_home
    if entry == alienhome:
        raise AlienException("alien_remove", "UserDeleteHome", entry)
    if not entry in alienhome:
        raise AlienException("alien_remove", "UserDeletePermit", entry)

    content = alien_ls(entry)
    if len(content) > 1:
        raise AlienException("delete", "ContentNotFile", entry)
    fileinfo = alien_ls(entry)[0]
    if not fileinfo.GetUserRights().IsFile():
        raise AlienException("delete", "ContentNotFile", entry)
    os.system('alien_rm %s' %(entry))

def alien_remove_dir(entry):
    """
    Recursively remove directory on alien
    Allows to delete content only inside home
    """
    if not alien_availability_test():
        raise AlienException("alien_find", "AlienNotAvailable", "")
    alienhome = alien_home
    if entry == alienhome:
        raise AlienException("alien_remove", "UserDeleteHome", entry)
    if not entry in alienhome:
        raise AlienException("alien_remove", "UserDeletePermit", entry)
    content = alien_ls(entry)
    for ien in content:
        if ien.GetUserRights().IsFile():
            alien_remove_file(entry)
        else:
            alien_remove_dir(entry)
    os.system("alien_rmdir %s" %(entry))

def alien_remove(entry):
    """
    Wrapper for alien_rm:

    - object is file: Remove single file
    - objet is directory: recursively remove directory
    """
    if not alien_availability_test():
        raise AlienException("alien_find", "AlienNotAvailable", "")
    alienhome = alien_home
    if entry == alienhome:
        raise AlienException("alien_remove", "UserDeleteHome", entry)
    if not entry in alienhome:
        raise AlienException("alien_remove", "UserDeletePermit", entry)

    content = alien_ls(entry)
    if len(content) == 1:
        # check for file
        if content[0].GetUserRights().IsFile():
            alien_remove_file(entry)
        else:
            alien_remove_dir(entry)
    elif len(content) > 1:
        alien_remove_dir(entry)

def alien_copy_file(source, target):
    """
    Copy single file from/to alien
    """
    if not alien_availability_test():
        raise AlienException("alien_find", "AlienNotAvailable", "")
    isFile = True
    targetOK = True
    if "alien://" in source:
        isFile = alien_is_file(source.replace("alien://",""))
    else:
        isFile =  os.path.isfile(source)
        if not alien_home() in target.replace("alien://",""):
            targetOK = False
    if not isFile:
        raise AlienException("alien_copy_file", "ContentNotFile", source)
    if not targetOK:
        raise AlienException("alien_copy_file", "TargetUserPermission", source)
    dirname = os.path.dirname(os.path.abspath(target))
    if not os.path.exists(dirname):
        os.makedirs(dirname, 0755)
    os.system("alien_cp %s %s" %(source, target))

def alien_find(basedir, pattern):
    """
    Find files in alien file catalogue

    Returns a python list of files found
    """
    if not alien_availability_test():
        raise AlienException("alien_find", "AlienNotAvailable", "")
    files = getstatusoutput("alien_find %s %s" %(basedir, pattern))[1].split("\n")
    result = []
    for found in files:
        if not basedir in found:
            continue
        result.append(found)
    return result

def alien_checksum(filename):
    """
    Get the checksum of a file in the alien file catalogue
    """
    workfilename = filename
    if "alien://" in workfilename:
        workfilename = workfilename.replace("alien://","")
    statusflag,alienresult = getstatusoutput("gbbox md5sum %s" %(workfilename))
    if statusflag != 0:
        raise AlienException("alien_checksum", "gbbox md5sum failed", filename)
    return alienresult.split("\t")[0]

def alien_whoami():
    """
    Return alien username
    """
    if not alien_availability_test():
        raise AlienException("alien_find", "AlienNotAvailable", "")
    return getstatusoutput("alien_whoami")[1].lstrip()

def alien_home():
    """
    Return users home directory
    """
    if not alien_availability_test():
        raise AlienException("alien_find", "AlienNotAvailable", "")
    username = alien_whoami()
    return "/alice/cern.ch/user/%s/%s" %(username[0], username[1])

def alien_availability_test():
    """
    Check whether alien is available
    """
    result = True
    FNULL = open(os.devnull, 'w')
    try:
        spcall(["alien_cp", "-h"], stdout = FNULL, stderr = FNULL)
    except OSError:
        result = False
    FNULL.close()
    return result
