'''
Created on Jan 19, 2015

@author: markus
'''

class UserRights(object):
    
    def __init__(self, userrightstring):
        self.__userrightstring = userrightstring
        
    def IsDirectory(self):
        return self.__userrightstring[0] == 'd'
    
    def IsFile(self):
        return not self.IsDirectory()
    
    def OwnerCanRead(self):
        return self.__userrightstring[1] == 'r'
    
    def OwnerCanWrite(self):
        return self.__userrightstring[2] == 'w'

    def OwnerCanExecute(self):
        return self.__userrightstring[3] == 'x'

    def GroupCanRead(self):
        return self.__userrightstring[4] == 'r'
    
    def GroupCanWrite(self):
        return self.__userrightstring[5] == 'w'

    def GroupCanExecute(self):
        return self.__userrightstring[6] == 'x'
    
    def AllCanRead(self):
        return self.__userrightstring[7] == 'r'
    
    def AllCanWrite(self):
        return self.__userrightstring[8] == 'w'

    def AllCanExecute(self):
        return self.__userrightstring[9] == 'x'
    
class EntryTime(object):
    
    def __init__(self, month, day, timeofday):
        self.__mont = month
        self.__day = day
        mytime = timeofday.split(":")
        self.__hour = mytime[0]
        self.__minute = mytime[1]
    
class AlienFileCatalogueEntry(object):
    
    def __init__(self, name):
        self.__entryname            = name
        self.__user                 = ''
        self.__group                = ''
        self.__filesize             = 0
        self.__userrights           = None
        self.__time                 = None
        
    def SetUserRights(self, userRightString):
        self.__userrights = UserRights(userRightString)
        
    def GetUserRights(self):
        return self.__userrights
    
    def SetTime(self, objecttime):
        self.__time = objecttime
        
    def GetTime(self):
        return self.__time
    
    def GetName(self):
        return self.__entryname
    
    def SetUser(self, user):
        self.__user = user
    
    def SetGroup(self, group):
        self.__group = group
        
    def GetUser(self):
        return self.__user
    
    def GetGroup(self):
        return self.__group
    
    def SetFilesize(self, fs):
        self.__filesize = fs
        
    def GetFilesize(self):
        return self.__filesize
    
class FileEntryDecoder(object):
    
    def __init__(self, line):
        self.__entry = self.Decode(line)
        
    def GetEntry(self):
        return self.__entry
    
    def Decode(self, line):
        infos = self.__TokenizeSafe(line, " ")
        entry = AlienFileCatalogueEntry(infos[7])
        entry.SetUserRights(infos[0])
        entry.SetUser(infos[1])
        entry.SetGroup(infos[2])
        entry.SetFilesize(int(infos[3]))
        entry.SetTime(EntryTime(infos[4], infos[5], infos[6]))
        return entry
    
    def __TokenizeSafe(self, inputstring, token):
        """
        Safe method to tokenize a string with delimiter, ignoring multiple occurrence of the
        same delimiter
        """
        result = []
        workingstring = inputstring
        running = True
        while running:
            nextfound = workingstring.find(token)
            if nextfound == -1:
                if len(workingstring):
                    result.append(workingstring)
                running = False
                break
            entry = workingstring[0:nextfound]
            if len(entry):
                result.append(entry)
            workingstring = workingstring[nextfound+1:]
        return result
    