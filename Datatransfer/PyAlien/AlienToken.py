'''
Created on Jan 27, 2015

Python representation of an alien token

@author: markus
'''

from datetime import datetime

class AlienToken(object):
    '''
    Class containing alien token information
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.__user             = ""
        self.__host             = ""
        self.__port             = None
        self.__port2            = None
        self.__pwd              = ""
        self.__nonce            = ""
        self.__sid              = ""
        self.__enc              = ""
        self.__expires          = ""
        self.__isValid          = ""
        
    def SetHost(self, hostname):
        self.__host = hostname
        
    def SetUser(self, user):
        self.__user = user
    
    def SetPort(self, port):
        self.__port = port
        
    def SetPort2(self, port2):
        self.__port2 = port2
        
    def SetPwd(self, pwd):
        self.__pwd = pwd
        
    def SetNonce(self, nonce):
        self.__nonce = nonce
        
    def SetSid(self, sid):
        self.__sid = sid 
        
    def SetExpiringDate(self,dtime):
        self.__expires = dtime
        

    def SetEncryption(self, enc):
        self.__enc = enc
        
    def GetHost(self):
        return self.__host

    def GetUser(self):
        return self.__user
    
    def GetPort(self):
        return self.__port
    
    def GetPort2(self):
        return self.__port2
    
    def GetPwd(self):
        return self.__pwd
    
    def GetNonce(self):
        return self.__nonce
        
    def GetSid(self):
        return self.__sid
    
    def GetEncryption(self):
        return self.__enc
    
    def GetExpiringDate(self):
        return self.__expires   
    
    # Definition of properties
    Host        = property(GetHost, SetHost)
    User        = property(GetUser, SetUser)
    Port        = property(GetPort, SetPort)
    Port2       = property(GetPort2, SetPort2)
    Pwd         = property(GetPwd, SetPwd)
    Nonce       = property(GetNonce, SetNonce)
    Sid         = property(GetSid, SetSid)
    Encryption  = property(GetEncryption, SetEncryption)
    ExpiryDate  = property(GetExpiringDate, SetExpiringDate)
    
    def IsValid(self):
        """
        Definition of validity:
        Current time has to be smaller than expiring time
        """
        return datetime.now() < self.__expires