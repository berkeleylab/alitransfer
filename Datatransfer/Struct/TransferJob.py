'''
Created on 24.12.2014

Database representation of a complete transfer job

@author: markusfasel
'''

from Datatransfer.Struct.FileInfo import FileInfo
import time
import datetime
from time import struct_time

class JobCreationDate(object):
    '''
    Time structure used in the transfer job to monitor the creation date.
    Basically a wrapper around struct_time. The structure is JSON-serializable
    '''

    def __init__(self, timestruct = None):
        '''
        Constructor
        '''
        self.__timestruct = timestruct
        #self.__dict__ = self.__dict__()

    def __str__(self):
        '''
        Create string representation
        '''
        return time.strftime("%Y/%m/%d, %H:%M:%S", self.__timestruct)

    def __dict__(self):
        '''
        Write structure to dictionary
        '''
        return {"year":self.__timestruct[0], "month":self.__timestruct[1], "day":self.__timestruct[2], "hour":self.__timestruct[3], "minute":self.__timestruct[4], "second":self.__timestruct[5]}

    def FromDict(self, source):
        '''
        Build structure from dictionary
        '''
        daytimetuple = datetime.date(source["year"], source["month"], source["day"]).timetuple() 
        timetuple = (source["year"], source["month"], source["day"], source["hour"], source["minute"], source["second"],daytimetuple[6],daytimetuple[7],daytimetuple[8])
        self.__timestruct = struct_time(timetuple)
        
    def Set(self, timestruct):
        '''
        Change timestamp
        '''
        self.__timestruct = timestruct


class TransferJob(object):
    '''
    Database representation of the transfer job
    Information stored in the database are defined in the constructor
    '''

    def __init__(self, name):
        '''
        Constructor
        '''
        self.__id               = None
        self.__name             = name
        self.__issuer           = ''
        self.__creationDate     = None
        self.__status           = "PEND";
        self.__files            = []

    def AddFile(self, sourcelocation, targetlocation):
        '''
        Add new file to the transfer job. Source and target location need to be specified.
        Certain tags in the names specify the type of transfer
        '''
        self.__files.append(FileInfo(transferjobname=self.__name, source=sourcelocation, target=targetlocation))

    def SetID(self, dbid):
        '''
        Set transfer job ID
        used for the communication with the MongoDB
        '''
        self.__id = dbid

    def SetCreationDate(self, timestruct):
        '''
        Set creation time of the transfer
        '''
        self.__creationDate = JobCreationDate(timestruct)

    def SetIssuer(self, username):
        '''
        Set name of the user who issued the transfer
        '''
        self.__issuer = username

    def SetStatus(self, status):
        '''
        Change job status
        Allowed are PEND, RUN, DONE, ERROR
        '''
        if not status in ["PEND", "RUN", "DONE", "ERROR"]:
            return
        self.__status = status

    def GetStatus(self):
        '''
        Return status code
        '''
        return self.__status

    def GetIssuer(self):
        '''
        Return name of the user issued the transfer
        '''
        return self.__issuer

    def GetCreationDate(self):
        '''
        Return creation date
        '''
        return self.__creationDate

    def GetListOfFiles(self):
        '''
        Return list of files connected to the transfer job
        '''
        return self.__files

    def GetID(self):
        '''
        Return ID of the entry in the MongoDB
        '''
        return self.__id

    def GetPercentageFinal(self):
        """
        Get Percentage of jobs in a final stage (DONE or ERROR)
        """
        nall = len(self.__files)
        nfinished = 0
        for myfile in self.__files:
            fstat = myfile.GetStatus()
            if fstat == "DONE" or fstat == "ERROR":
                nfinished += 1
        return float(nall) / float(nfinished)

    def GetFile(self, sourcefilename):
        """
        Access to source file info
        """
        result = None
        for myfile in self.__files:
            if myfile.GetSourceLocation() == sourcefilename:
                result = myfile
                break
        return result

    def SetFileStatus(self, sourcefilename, newstatus):
        """
        Change status of a single file
        """
        for myfile in self.__files:
            if myfile.GetSourceLocation() == sourcefilename:
                myfile.SetStatus(newstatus)


    def Serialize(self):
        """
        Prepare object for MongoDB
        """
        listoffiles = [];
        for myfile in self.__files:
            listoffiles.append(myfile.Serialize())
        return {"Name":self.__name, "Issuer":self.__issuer, "CreationDate":self.__creationDate.__dict__(), "Status":self.__status, "Files":listoffiles}

    def Initialize(self, mydict):
        """
        Recover object from MongoDB
        """
        self.__id = mydict["_id"]
        self.__name = mydict["Name"]
        self.__issuer = mydict["Issuer"]
        self.__creationDate = JobCreationDate()
        self.__creationDate.FromDict(mydict["CreationDate"])
        self.__status = mydict["Status"]
        for myfile in mydict["Files"]:
            myinfo = FileInfo()
            myinfo.Initialise(myfile)
            self.__files.append(myinfo)


    def Print(self):
        '''
        Print transfer job status
        '''
        print "Transfer job:               %s" %(self.__name)
        print "Issuer:                     %s" %(self.__issuer)
        print "Status:                     %s" %(self.__status)
        print "Created:                    %s" %(self.__creationDate)
        print "Status:                     %s" %(self.__status)
        print "Files:"
        print "============================================="
        for f in self.__files:
            f.Print()
