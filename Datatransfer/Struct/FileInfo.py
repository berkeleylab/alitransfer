'''
Created on 24.12.2014

information about file, used by the transfer job and by the 
transfer queue entry

@author: markusfasel
'''

class FileInfo(object):
    '''
    classdocs
    '''

    def __init__(self, dbid=None, transferjobname=None, source=None, target=None):
        '''
        Constructor
        '''
        self.__id = dbid
        self.__transferjob = transferjobname
        self.__sourcelocation = source
        self.__targetlocation = target
        self.__status = "PEND"
        
    def SetSourceLocation(self, source):
        self.__sourcelocation = source
        
    def SetTargetLocation(self, target):
        self.__targetlocation = target
        
    def SetTransferJob(self, transferjobname):
        self.__transferjob = transferjobname
        
    def SetID(self, dbid):
        self.__id = id
        
    def GetSourceLocation(self):
        return self.__sourcelocation
    
    def GetTargetLocation(self):
        return self.__targetlocation
    
    def GetTransferJob(self):
        return self.__transferjob
    
    def SetStatus(self, status):
        if not status in ["PEND", "RUN", "DONE", "ERROR"]:
            return
        self.__status = status
        
    def GetStatus(self):
        return self.__status
    
    def GetID(self):
        return self.__id
    
    def Serialize(self):
        return {"TransferJob":self.__transferjob, "SourceLocation": self.__sourcelocation, "TargetLocation": self.__targetlocation, "Status":self.__status}
    
    def Initialise(self, mydict):
        if "_id" in mydict.keys():
            self.__id               = mydict["_id"]
        self.__transferjob      = mydict["TransferJob"]
        self.__sourcelocation   = mydict["SourceLocation"]
        self.__targetlocation   = mydict["TargetLocation"]
        self.__status           = mydict["Status"]
        
    def Print(self):
        '''
        Print file information
        '''
        print "In[%s] Out[%s] of Job %s with status %s" %(self.__sourcelocation, self.__targetlocation, self.__transferjob, self.__status)
