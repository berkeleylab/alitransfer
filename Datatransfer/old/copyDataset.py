#! /usr/bin/env python
# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''
:since: Sept 09, 2014
:author: Markus Fasel
:contact: mfasel@lbl.gov
:organization: Lawrence Berkeley National Laboratory
'''

from dataMover import ProductionMover
import sys

class FieldError(Exception):
    
    def __init__(self, field):
        self.__field = field
        
    def __str__(self):
        return "Field %s not initialised" %(self.__field)
    
class TransferConfiguration:
    
    def __init__(self):
        self.__dataset = ""
        self.__alienpath = ""
        self.__outputdir = ""
        self.__productionpass = 0
        self.__runlist = []
        
    def SetDataset(self, dataset):
        self.__dataset = dataset
        
    def SetAlienpath(self, alienpath):
        self.__alienpath = alienpath
        
    def SetOutputdir(self, outputdir):
        self.__outputdir = outputdir
    
    def SetProductionPass(self, productionpass):
        self.__productionpass = productionpass
        
    def AddRun(self, run):
        self.__runlist.append(run)
        
    def GetDataset(self):
        if not len(self.__dataset):
            return FieldError("dataset")
        return self.__dataset
    
    def GetAlienpath(self):
        if not len(self.__alienpath):
            raise FieldError("alienpath")
        return self.__alienpath
    
    def GetOutputdir(self):
        if not len(self.__outputdir):
            raise FieldError("outputdir")
        return self.__outputdir
    
    def GetRunlist(self):
        if not len(self.__runlist):
            raise FieldError("runlist")
        return self.__runlist
    
    def GetProductionPass(self):
        return self.__productionpass
    
    def GetRunstring(self):
        counter = 0
        result = ""
        for run in self.__runlist:
            result += "%s" %(run)
            if counter < len(self.__runlist):
                result += ","
        return result
    
    def Print(self):
        print "Transfer configuration:"
        print "================================="
        print "Dataset:              %s" %(self.__dataset)
        print "Alienpath:            %s" %(self.__alienpath)
        print "Production pass:      %d" %(self.__productionpass)
        print "Output location:      %s" %(self.__outputdir)
        print "Runlist:              %s" %(self.GetRunstring())
    
class ConfigKeyError(Exception):
    
    def __init__(self, key):
        self.__key = key
        
    def __str__(self):
        return "Key %s undefined" %(self.__key)

class ConfigReader:
    
    def __init__(self, filename):
        self.__filename = filename
        self.__cofiguration = TransferConfiguration()
        self.Process()

    def Process(self):
        reader = open(self.__filename, 'r')
        for line in reader:
            line = line.replace("\n", "")
            print line
            content=line.split(":")
            try:
                self.__ProcessKey(content[0], content[1])
            except ConfigKeyError, err:
                print err
        reader.close()
            
    def __ProcessKey(self, key, value):
        if key == "dataset":
            self.__cofiguration.SetDataset(value)
        elif key == "alienpath":
            self.__cofiguration.SetAlienpath(value)
        elif key == "outputdir":
            self.__cofiguration.SetOutputdir(value)
        elif key == "pass":
            self.__cofiguration.SetProductionPass(int(value))
        elif key == "runlist":
            runs = value.split(",")
            for run in runs:
                print run
                self.__cofiguration.AddRun("%09d" %(int(run)))
        else:
            raise ConfigKeyError(key) 
        
    def GetConfiguration(self):
        return self.__cofiguration
                       
def main():
    reader = ConfigReader(sys.argv[1])
    config = reader.GetConfiguration()
    try:
        task = ProductionMover(dataset = config.GetDataset(), alienpath = config.GetAlienpath(), outputbase = config.GetOutputdir(), productionpass = config.GetProductionPass())
        for run in config.GetRunlist():
            task.AddRun(str(run))
        task.Process()
    except FieldError, e:
        print e
        print "Transfer stopped"

if __name__ == '__main__':
    main()