# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''
:since: Sept 08, 2014
:author: markusfasel
:contact: mfasel@lbl.gov
:organization: Lawrence Berkeley National Laboratory
'''

from ROOT import TFile, TGrid, gGrid
from commands import getstatusoutput
import os, subprocess, zipfile

def ByteToGigabyte(sizeinbytes):
    return sizeinbytes/(1024*1024*1024)

class Tester:
    
    def __init__(self, filename):
        self._result = None
        
    def _FindTree(self, filename, treename):
        inputfile = TFile.Open(filename)
        if not inputfile or input.IsZombie():
            return False
        result = True
        try:
            mytree = inputfile.Get(treename)
            if not mytree:
                result = False
        except:
            result = False
        file.Close()
        return result

    def GetResult(self):
        return self.__result

class ESDTester(Tester):
    
    def __init__(self, filename):
        self._result = self.__DoOpen(filename)
        
    def __DoOpen(self, filename):
        return self._FindTree(filename, "esdTree")

class AODTester(Tester):
    
    def __init__(self, filename):
        self._result = self.__DoOpen(filename)
        
    def __DoOpen(self, filename):
        return self._FindTree(filename, "aodTree")

class FileInfo:
    
    def __init__(self, filename, size, md5sum):
        self.__filename = filename
        self.__size = size
        self.__md5sum = md5sum
        
    def JSONString(self):
        return "{filename:%s, size:%s}" %(self.__filename, self.__size)

class DataCollection:
    
    def __init__(self, dataset):
        self.__dataset = dataset
        self.__listoffiles = []
        
    def AddFile(self, filename):
        self.__listoffiles.append(filename)
        
    def GetListOfFiles(self):
        return self.__listoffiles
    
    def GetDataset(self):
        return self.__dataset
    
    def JSONString(self):
        jsonstring = "{dataset:%s, {filelist:[" %(self.__dataset)
        counter = 0
        for myfile in self.__listoffiles:
            jsonstring += "%s" %(myfile)
            if counter < len(self.__listoffiles):
                jsonstring += ","
            counter += 1
        jsonstring += "]}}"
        return jsonstring
    
    def WriteToFile(self, filename):
        filewriter = open(filename)
        filewriter.write(self.JSONString())
        filewriter.close()
    
class DataMover:
    
    def __init__(self, dataset):
        self.__alienpath = None
        self.__outputdir = None
        self.__searchpattern = None
        self.__datacollection = DataCollection(dataset)
        
    def Print(self):
        print "New datamover:"
        print "====================="
        print "Dataset: %s" %(self.__datacollection.GetDataset())
        print "Alienpath: %s" %(self.__alienpath)
        print "Output Location: %s" %(self.__outputdir)
        print "Search pattern: %s" %(self.__searchpattern)
        if self.__datacollection:
            datajson = self.__datacollection.JSONString()
            print "Data collection: %s" %(datajson)
    
    def SetPathOnAlien(self, alienpath):
        self.__alienpath = alienpath
        
    def SetOutputDir(self, outputdir):
        self.__outputdir = outputdir
    
    def SetSearchPattern(self, searchpattern):
        self.__searchpattern = searchpattern
        
    def Run(self):
        filelist = self.QueryFiles()
        for myfile in filelist:
            filepattern = myfile.replace("alien://%s" %(self.__alienpath),"")
            if filepattern.startswith("/"):
                # strip away beginning /
                filepattern = filepattern[1:]
            outputfile = "%s/%s" %(self.__outputdir, filepattern)
            outputbasedir = os.path.dirname(outputfile)
            if not os.path.exists(outputbasedir):
                os.makedirs(outputbasedir, 0755)
            self.DoFile(myfile, outputfile)
        self.__datacollection.WriteToFile("%s/collection_%s.json" %(self.__outputdir, self.__datacollection.GetDataset()))
            
    def InspectZipfile(self, testfile):
        zfile = None
        try:
            zfile = zipfile.ZipFile(testfile, mode = 'r')
        except:
            # Zipfile not readable - bad
            return False
        content = zfile.infolist()
        status = True
        for f in content:
            tester = None
            if f.filename is "AliESDs.root":
                tester = ESDTester("%s#%s" %(testfile, f.filename))
            elif f.filename is "AliAOD.root":
                tester = AODTester("%s#%s" %(testfile, f.filename))
            else: 
                continue
            if not tester.GetResult():
                status = False
        return status
        
    def DoFile(self, inputfile, outputfile):
        #transport file
        commandstring = "source /usr/share/Modules/init/bash;"
        commandstring += "module use /project/projectdirs/alice/software/modulefiles/;"
        commandstring += "module load alice/alien/default;"
        commandstring += "source /tmp/gclient_env_$UID;"
        commandstring += "alien_cp %s %s;" %(inputfile, outputfile)
        subprocess.call(commandstring, shell=True)
        #do basic tests        
        if self.InspectZipfile(outputfile):
            #obtain md5sum of the file
            md5sum = self.__GetMD5sum(outputfile)
            self.__datacollection.AddFile(FileInfo(outputfile,ByteToGigabyte(os.path.getsize(outputfile)), md5sum))
        else:
            # File corrupted, delete it
            os.remove(outputfile)
        
    def __GetMD5sum(self, filename):
        results = getstatusoutput("md5sum %s" %(filename))
        return results[1].split(" ")[0]        
        
    def QueryFiles(self):
        TGrid.Connect("alien://")
        gridresults = gGrid.Query(self.__alienpath, self.__searchpattern)
        filelist = []
        resultIter = gridresults.MakeIterator()
        entry = resultIter.Next()
        while entry:
            filelist.append(entry.GetValue("turl").String().Data())
            entry = resultIter.Next()
        return filelist
    
class ProductionMover:
    def __init__(self, dataset, alienpath, outputbase, productionpass):
        self.__dataset = dataset
        self.__alienpath = alienpath
        self.__outputbase = outputbase
        self.__productionpass = productionpass 
        self.__listofruns = []
        
    def AddRun(self, run):
        self.__listofruns.append(run)
        
    def Process(self):
        for run in self.__listofruns:
            datamover = DataMover("%s_%s" %(self.__dataset, run))
            datamover.SetPathOnAlien("%s/%s" %(self.__alienpath, run))
            datamover.SetSearchPattern("ESDs/pass%d/%%%s%%/root_archive.zip" %(self.__productionpass, run))
            datamover.SetOutputDir("%s/%s" %(self.__outputbase, run))
            datamover.Print()
            datamover.Run()