'''
Created on Mar 2, 2015

Worker implementation of a transfer process as thread, steered by a thread manager

@author: markus
'''

from threading import Thread
from Datatransfer.TransferQueue.CopyHandler import CopyHandlerFactory
import time

class TransferWorker(Thread):
    '''
    Implementation of a worker as thread
    '''

    def __init__(self, threadid, ttmanager):
        '''
        Constructor
        '''
        Thread.__init__(self)
        self.__threadID = threadid
        self.__threadmanager = ttmanager
        self.__running = False
        self.__idle = True
        self.__entryToProcess = None            # Next entry to be handled
        self.__entryHandled = None              # Previous entry, already processed, for database update
        self.__sleeplength = 1
        self.__debugmode = False

    def run(self):
        '''
        Run loop:
        checks for new files
        '''
        Thread.run(self)
        if self.__debugmode:
            print "Thread running"
        self.__running = True
        counter = 0
        while self.__running:
            if self.__debugmode:
                print "Thread %d, loop iteration %d, running %s" %(self.__threadID, counter, "yes" if self.__running else "no")
            counter += 1
            try:
                # Only process entry if the previous entry is already handled by someone else
                if self.__entryToProcess and not self.__entryHandled:
                    self.__idle = False
                    status = CopyHandlerFactory.DoCopy(self.__entryToProcess.GetSourceLocation(), self.__entryToProcess.GetTargetLocation(), self.__threadmanager.GetWorkerSettings())
                    if status:
                        # copy successfull
                        self.__entryToProcess.SetStatus("DONE")
                    else:
                        # failure - put back on the queue, but let the manager handle this
                        self.__entryToProcess.SetStatus("PEND")
                    self.__entryHandled = self.__entryToProcess
                    self.__entryToProcess = None
                    self.__idle = True
                else:
                    # wait one minute to check again if I have work to do
                    if self.__debugmode:
                        print "Thread %d, idle, sleeping %d sec" %(self.__threadID, self.__sleeplength)
                time.sleep(self.__sleeplength)
            except Exception as e:
                self.__running = False
                print "Thread %d, stopped due to exception: %s" %(self.__threadID, str(e))
        if self.__debugmode:
            print "Thread %d, stopped" %(self.__threadID)

    def IsIdle(self):
        '''
        Check if worker is idle (not processing any file at the moment)
        '''
        return self.__idle
    
    def SetIdle(self, idle):
        '''
        Mark worker as idle from outside
        '''
        self.__idle = idle

    def IsBusy(self):
        '''
        Check if worker is busy (processing a file at the moment)
        '''
        return self.__running and not self.__idle

    def IsRunning(self):
        '''
        Check if worker is running (processing a thread at the moment)
        '''
        return self.__running

    def SetRunning(self, running):
        '''
        Set the running status to true
        '''
        if self.__debugmode:
            print "Thread %d, running set to %s" %(self.__threadID, "True" if running else "False")
        self.__running = running

    def SetNewEntry(self, newentry):
        '''
        Set new entry to process
        '''
        self.__entryToProcess = newentry

    def GetLastEntry(self):
        '''
        Access last entry
        '''
        myentry = self.__entryHandled
        self.__entryHandled = None
        return myentry

    def SetSleepLength(self, sleeplength):
        '''
        Adjust sleeplength in case of long time inactivity on the queue
        '''
        self.__sleeplength = sleeplength
        
    def SetDebugMode(self, dodebug):
        '''
        Defining debug mode
        '''
        self.__debugmode = dodebug
