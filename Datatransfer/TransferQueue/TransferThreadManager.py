'''
Created on Mar 2, 2015

Manager class handling transfer workers

@author: markus
'''
from Datatransfer.TransferQueue.TransferWorker import TransferWorker
from Datatransfer.TransferQueue.WorkerSettings import WorkerSettings

class TransferThreadManager(object):
    '''
    Manager class of the transfer workers
    Starts and stops worker threads
    Distributes payload
    '''

    def __init__(self, nthread, transferdb):
        '''
        Constructor
        '''
        self.__threadpool = []
        self.__newthreadID = 0
        self.__debugmode = False
        for ithread in range(0, nthread):
            self.__threadpool.append(TransferWorker(self.__newthreadID, self))
            self.__newthreadID += 1
        self.__transferdb = transferdb
        self.__workerparams = WorkerSettings()
        self.__running = False

    def StartThreads(self):
        '''
        Start all workers
        '''
        for ithread in self.__threadpool:
            ithread.start()
        self.__running = True

    def StopThreads(self):
        '''
        Stop all workers
        '''
        if self.__debugmode:
            print "Thread manager: Stopping all threads"
        for ithread in self.__threadpool:
            ithread.SetRunning(False)
        for ithread in self.__threadpool:
            ithread.join()
        self.__running = False

    def StopThread(self, threadtostop):
        '''
        Stop thread
        '''
        threadtostop.SetRunning(False)
        self.__threadpool.remove(threadtostop)

    def StopThreadByID(self, threadID):
        '''
        Stop only a given thread
        '''
        if threadID < len(self.__threadpool):
            self.StopThread(self.__threadpool[threadID])

    def AddWorkers(self, nworkers):
        '''
        Add new workers to the threadpool
        '''
        for i in range(0, nworkers):
            newworker = TransferWorker(self.__newthreadID, self)
            if self.__debugmode:
                newworker.SetDebugMode(True)
            newworker.SetRunning(True)
            self.__threadpool.append(newworker)
            self.__newthreadID += 1

    def StopNWorkers(self, nworkers):
        '''
        Stop a given amount of workers
        first check idle workers
        '''
        nworkerstostop = nworkers if nworkers <= len(self.__threadpool) else len(self.__threadpool)
        if nworkerstostop == 0:
            return
        nstopped = 0
        listOfIdles = self.__GetListOfIdleWorkers()

        # First take workers which are idle
        for worker in listOfIdles:
            self.StopThread(worker)
            nstopped += 1
            if nstopped >= nworkerstostop:
                break

        # now send also stop signal to non-idle workers
        for i in range(0, nworkerstostop - nstopped):
            self.StopThreadByID(i)

    def GetWorkerSettings(self):
        """
        Access to worker params
        """
        return self.__workerparams

    def HandleWork(self):
        '''
        Determines workers which are idle, handles processed entries, and puts new load,
        if available, to the workers
        '''
        if not len(self.__threadpool):
            self.__running = False 
            return
       
        listOfIdles = self.__GetListOfIdleWorkers()
        nbusy = len(self.__threadpool) - len(listOfIdles)
        for worker in listOfIdles:
            lastentry = worker.GetLastEntry()
            if lastentry:
                self.__transferdb.UpdateFileStatusInDatabase(lastentry)
            # Put new work to the slaves
            if self.__debugmode:
                print "Reading next entry from the queue"
            newentry = self.__transferdb.GetNextEntryInQueue()
            if newentry:
                worker.SetNewEntry(newentry)
                worker.SetIdle(False)
                nbusy += 1
        return nbusy
    
    def SetDebugMode(self, dodebug):
        """
        Setting itself and the workers to debug mode
        """
        self.__debugmode = dodebug
        if self.__debugmode:
            for mythread in self.__threadpool:
                mythread.SetDebugMode(dodebug)

    def SetSleepLength(self, sleeplength):
        '''
        Adjust sleeplength in case of long time inactivity
        '''
        for mythread in self.__threadpool:
            mythread.SetSleepLength(sleeplength)

    def __GetListOfBusyWorkers(self):
        '''
        Determines the workers which are currently busy
        '''
        listofbusy = []
        for worker in self.__threadpool:
            if worker.IsBusy():
                listofbusy.append(worker)
        return listofbusy

    def __GetListOfIdleWorkers(self):
        '''
        Determine workers which are idle and can get new payload
        '''
        listofidles = []
        for i in self.__threadpool:
            # remove workers which are not running
            if not i.IsRunning():
                self.HandleClosedWorker(i)
                self.__threadpool.remove(i)
            if i.IsIdle():
                listofidles.append(i)
        return listofidles
    
    def HandleClosedWorker(self, worker):
        """
        In case the status is still marked as "RUN", put the entry back to the 
        queue
        """
        entry = worker.GetLastEntry()
        if entry and entry.GetStatus() == "RUN":
            # put entry back to the queue
            entry.SetStatus("PEND")
            self.__transferdb.UpdateFileStatusInDatabase(entry)

    def IsRunning(self):
        """
        Check whether we have running threads
        """
        return self.__running