#! /usr/bin/env python
'''
Created on Jan 26, 2015

Class implementing the transfer daemon on the worker side
Copy processes are executed via threads, steered by a thread
manager. The daemon itself keeps control of the thread manager
and adjusts it if necessary.

@author: markus
'''

import time, os, sys, getopt, socket

if __name__ == "__main__":
    """
    Append source directory to PTYHONPATH if not yet done
    """
    sourcedir = os.path.abspath(os.path.dirname(sys.argv[0]))
    sourcedir = sourcedir.replace("/%s" %(os.path.join("Datatransfer", "TransferQueue")),"")
    if not sourcedir in sys.path:
        print "Adding %s to the PYTHONPATH" %(sourcedir)
        sys.path.append(sourcedir)

from Datatransfer.TransferQueue.TransferThreadManager import TransferThreadManager
from Datatransfer.TransferDB.TransferDBConnector import TransferDBConnector

class TransferDaemon(object):
    '''
    Transfer daemon implementation:
    Initialises and closes the transfer running, steers the connection with
    the database, forces the update of load and adjusts sleep time. The transfers
    themselves are executed in threads, steered by a thread manager.
    '''

    def __init__(self, databaseconnection, nthread):
        '''
        Constructor
        '''
        self.__transferDB = TransferDBConnector(databaseconnection)
        self.__shutdowndir = "/project/projectdirs/alice/hpc"
        self.__debugmode = False
        self.__transferManager = TransferThreadManager(nthread, self.__transferDB)
        self.__transferManager.StartThreads()

    def SetShutdownDir(self, path):
        """
        Specify directory in which to look for the backdoor shutdown signal
        """
        self.__shutdowndir = path
        
    def SetDebugMode(self, dodebug):
        """
        Setting the debug mode of the daemon
        If true, then debug statements from the daemon, the thread manager and the workers will be printed
        """
        self.__debugmode = dodebug
        self.__transferManager.SetDebugMode(self.__debugmode)

    def Backdoor_shutdown(self):
        """
        A backdoor mechanism to shut down the daemon, i.e. for a software update or when the
        database connection changes, from outside. Shutdown signal for the moment is the presence
        of the file "shutdowntransfer" in the folder /project/projectdirs/alice/hpc/". Each worker
        will then create a file HOSTNAMEshutdown. With this method it is possible to stop transfer
        nodes without interupting the running processes.

        A complementary method in the future will be done with status flags in a document collection
        for central services in the MongoDB
        """
        if os.path.exists(os.path.join(self.__shutdowndir, "shutdowntransfer")):
            if self.__debugmode:
                print "Transfer daemon: Shutdown request found in %s" %(self.__shutdowndir)
            shutdownout = os.path.join(self.__shutdowndir, "%sshutdown" %(socket.gethostname()))
            self.__transferManager.StopThreads()
            outputtoken = open(shutdownout, 'w')
            outputtoken.write("Worker %s shutdown\n" %(socket.gethostname()))
            outputtoken.close()
            return True
        if self.__debugmode:
            print "Transfer daemon: No shutdown request in %s" %(self.__shutdowndir)
        return False

    def Run(self):
        """
        Run infinity loop processing entries in the transfer queue
        if queue is empty, wait for a minute
        """
        timer = time
        sleeplength = 1
        while True:
            if self.Backdoor_shutdown() or not self.__transferManager.IsRunning():
                break
            if self.__transferDB.HasJobs():
                # Wakeup threads
                if self.__debugmode:
                    print "More unhandled jobs in the queue"
                self.__transferManager.SetSleepLength(1)
                self.__transferManager.HandleWork()
                sleeplength = 1
            else:
                # not busy, wait for a longer period, increasingly
                if sleeplength < 601:
                    sleeplength += 60
                    self.__transferManager.SetSleepLength(sleeplength)
            timer.sleep(sleeplength)

if __name__ == "__main__":
    nthread = 5
    shutdowdir=None
    debugmode = False
    if len(sys.argv) > 2:
        opt,arg = getopt.getopt(sys.argv[2:], "n:s:d", [])
        for o,a in opt:
            if o == "-n":
                nthread = int(a)
            elif o == '-s':
                shutdowndir = str(a)
            elif o == '-d':
                debugmode = True
    mydaemon = TransferDaemon(sys.argv[1], nthread)
    if shutdowndir:
        mydaemon.SetShutdownDir(shutdowndir)
    if debugmode:
        mydaemon.SetDebugMode(True)
    mydaemon.Run()
